<?php
    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    
    $sheep = new Animal("shaun");
    $kodok = new Frog("Buduk");
    $sungokong = new Ape("Kera Sakti");

    echo "Name: " . $sheep->namaHewan . "<br>"; // "shaun"
    echo "Jumlah kaki: " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded: " . $sheep->cold_blooded . "<br><br>"; // "no"

    echo "Name: " . $kodok->namaHewan . "<br>";
    echo "Jumlah kaki: " . $kodok->legs . "<br>";
    echo "Cold Blooded: " . $kodok->cold_blooded . "<br>";
    echo "Jump: " . $kodok->jump() . "<br><br>";

    echo "Name: " . $sungokong->namaHewan . "<br>";
    echo "Jumlah kaki: " . $sungokong->legs . "<br>";
    echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>";
    echo "Yell: " . $sungokong->yell();

?>