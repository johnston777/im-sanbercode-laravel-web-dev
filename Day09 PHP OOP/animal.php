<?php
    class Animal
    {
        public $namaHewan;
        public $legs = 4;
        public $cold_blooded = "no";

        public function __construct($name)
        {
            $this->namaHewan = $name;
        }
    };
?>