<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar ()
    {
        return view("halaman.signup");
    }

    public function home (Request $request)
    {
        $namaDepan = $request->input("fname");
        $namaBelakang = $request->input("lname");
        return view("halaman.home",["namaDepan" => $namaDepan, "namaBelakang" => $namaBelakang]);
    }
}
