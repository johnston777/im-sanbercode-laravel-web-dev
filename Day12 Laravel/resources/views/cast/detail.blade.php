@extends('layouts.master')

@section('title')
Halaman Detail Pemeran
@endsection

@section('content')
<h1>{{$cast->nama}}</h1>
<h2>Umur {{$cast->umur}}</h2>
<p>{{$cast->bio}}</p> 
@endsection