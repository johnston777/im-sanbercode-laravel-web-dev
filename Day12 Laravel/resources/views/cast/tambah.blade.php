@extends('layouts.master')

@section('title')
Halaman Tambah Pemeran
@endsection

@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control @error('nama') is-invalid @enderror">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="text" name="umur" class="form-control @error('umur') is-invalid @enderror">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control @error('bio') is-invalid @enderror" rows=4></textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection