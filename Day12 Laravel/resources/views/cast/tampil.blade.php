@extends('layouts.master')

@section('title')
Halaman Tampil Pemeran
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <th scope="row">{{$key + 1}}</th>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="post">
                        @method("delete")
                        @csrf
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
              </tr>
        @empty
            <p>Data Pemeran Kosong</p>
        @endforelse

    </tbody>
  </table>
@endsection