@extends('layouts.master')

@section('title')
Buat Account Baru!
@endsection

@section('content')
<form action="/home" method="post">
    @csrf
    <label>First Name</label><br><br>
    <input type="text" name="fname"><br><br>
    <label>Last Name</label><br><br>
    <input type="text" name="lname"><br><br>
    <label>Gender:</label><br><br>
    <input type="radio" name="gender" value="Male">Male<br>
    <input type="radio" name="gender" value="Female">Female<br>
    <input type="radio" name="gender" value="Other">Other<br><br>
    <label>Nationality</label><br><br>
    <select name="nationality">
        <option value="indonesia">Indonesian</option>
        <option value="malaysian">Malaysian</option>
        <option value="singaporean">Singaporean</option>
    </select><br><br>
    <label>Language Spoken:</label><br><br>
    <input type="checkbox" value="bahasa_indonesia">Bahasa Indonesia<br>
    <input type="checkbox" value="english">English<br>
    <input type="checkbox" value="other">Other<br><br>
    <label>Bio:</label><br><br>
    <textarea name="bio" cols="50" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>
@endsection
