<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class,"index"]);
Route::get('/signup', [AuthController::class,"daftar"]);
Route::post('/home', [AuthController::class,"home"]);

Route::get('/data-tables', function(){
    return view("halaman.data-tables");
});
Route::get('/table', function(){
    return view("halaman.table");
});

// CRUD
// Create Data
Route::get('/cast/create', [CastsController::class,"create"]);
// Insert Data
Route::post('/cast', [CastsController::class,"store"]);
// Read Data
Route::get('/cast', [CastsController::class,"index"]);
Route::get('/cast/{id}', [CastsController::class,"show"]);
//Update Data
Route::get('/cast/{id}/edit', [CastsController::class,"edit"]);
Route::put('/cast/{id}', [CastsController::class,"update"]);
//Delete Data
Route::delete('/cast/{id}', [CastsController::class,"destroy"]);